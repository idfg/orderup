package com.appattack.orderup;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

public class ItemListAdapter extends BaseAdapter {
	private Context context;
	private String[] itemNameArray;
	private static LayoutInflater inflater = null;

	public ItemListAdapter(MenuCategoryActivity menuCategoryActivity,
			String[] itemNameArray) {
		this.context = menuCategoryActivity;
		this.itemNameArray = itemNameArray;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return itemNameArray.length;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public class Holder {
		TextView itemName;
		ImageView coverPhoto;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		Holder holder = new Holder();
		View rowView;
		rowView = inflater.inflate(R.layout.list_menu_item, null);

		holder.itemName = (TextView) rowView.findViewById(R.id.item_name);
		holder.coverPhoto = (ImageView) rowView.findViewById(R.id.item_cover);

		holder.itemName.setText(itemNameArray[position]);
		holder.coverPhoto.setScaleType(ScaleType.CENTER_CROP);

		rowView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				
			}
		});

		return rowView;
	}
}