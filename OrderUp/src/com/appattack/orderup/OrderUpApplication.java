package com.appattack.orderup;

import android.app.Application;

/**
 * Use a custom Application class to pass state data between Activities.
 */
public class OrderUpApplication extends Application {
	private Customer customer;
	
    public void setCustomerInfo(String name, String room, CustomerType type) {
    	customer = new Customer(name, room, type);
    }
    
    public Customer getCustomer() {
    	return customer;
    }
    
}