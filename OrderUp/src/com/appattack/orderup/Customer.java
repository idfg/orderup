package com.appattack.orderup;

public class Customer {

	private String name;
	private String room;
	private CustomerType type;
	
	public Customer(String name, String room, CustomerType type) {
		this.name = name;
		this.room = room;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public String getRoom() {
		return room;
	}

	public CustomerType getType() {
		return type;
	}
	
}
