package com.appattack.orderup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

public class MenuActivity extends Activity {

    protected static final String TAG = "MenuActivity";

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        
		Button beverage = (Button) findViewById(R.id.beverage);
		beverage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				startActivity(new Intent(MenuActivity.this, MenuCategoryActivity.class));
			}
		});
    }

}
