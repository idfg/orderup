package com.appattack.orderup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;

public class MainActivity extends Activity {

    protected static final String TAG = "MainActivity";

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

		Button next = (Button) findViewById(R.id.next);
		next.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				EditText nameEditText = (EditText) findViewById(R.id.name_edit_text);
				EditText roomEditText = (EditText) findViewById(R.id.room_edit_text);
				Switch guestPatientSwitch = (Switch)findViewById(R.id.guest_patient_switch);
				String name = nameEditText.getText().toString();
				String room = roomEditText.getText().toString();

				if (guestPatientSwitch.isChecked()) {
					Log.i(TAG, name + " " + room + " Patient" );
					((OrderUpApplication) getApplication()).setCustomerInfo(name, room, CustomerType.PATIENT);
					
				} else {
					Log.i(TAG, name + " " + room + " Guest" );
					((OrderUpApplication) getApplication()).setCustomerInfo(name, room, CustomerType.GUEST);
				}
				
				startActivity(new Intent(MainActivity.this, MenuActivity.class));
			}
		});

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
