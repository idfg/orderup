package com.appattack.orderup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Switch;

public class MenuCategoryActivity extends Activity {

    protected static final String TAG = "BeverageActivity";
    private ListView menuItemListView;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_category);
        
        
        
		menuItemListView = (ListView) findViewById(R.id.item_listview);
		menuItemListView.setAdapter(new ItemListAdapter(this, null));

        Button backButton = (Button) findViewById(R.id.back_button);
        backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				finishActivity();
			}

        });

        
    }

	private void finishActivity() {
		super.onBackPressed();
	}
}
